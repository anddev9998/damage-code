$(function(){
		
	// Lazy Load - https://github.com/ApoorvSaxena/lozad.js
    var observer = lozad('.lozad', {
        threshold: 0.1,
        load: function(el) {
            el.src = el.getAttribute("data-src");
            el.onload = function() {
	          el.classList.add('loaded');
              console.log("Success " + el.localName.toUpperCase() + " " + el.getAttribute("data-index") + " lazy loaded.")
            }
        }
    })
    observer.observe()
	
	// File upload
    $('.file-upload').change(function() {
        var filepath = this.value;
        var m = filepath.match(/([^\/\\]+)$/);
        var filename = m[1];
        $(this).parent('label').siblings('span').html(filename);
    });
    
});
function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main-panel").style.marginRight = "250px";
    document.getElementById("main-content").style.right = "250px";
    document.getElementById("header-content").style.right = "250px";
    document.getElementById("side-bar-open").style.display = "none";
    document.getElementById("side-bar-close").style.display = "block";
}

function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main-panel").style.marginRight = "0";
    document.getElementById("main-content").style.right = "0px";
    document.getElementById("header-content").style.right = "0px";
    document.getElementById("side-bar-open").style.display = "block";
    document.getElementById("side-bar-close").style.display = "none";
}